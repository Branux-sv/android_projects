package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    private double tmpResult = 0;
    private String strTmpResult = "";

    private boolean isSum = false, isRest = false, isMul = false, isDiv = false;

    //Controles
    TextInputEditText tilEntrada;
    Button  btn0;
    Button  btn1;
    Button  btn2;
    Button  btn3;
    Button  btn4;
    Button  btn5;
    Button  btn6;
    Button  btn7;
    Button  btn8;
    Button  btn9;
    Button  btnDelete;
    Button  btnPunto;
    Button  btnMas;
    Button  btnMenos;
    Button  btnMulti;
    Button  btnDivi;
    Button  btnCalcular;
    TextView tvHistorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Enlazar Controles
        tilEntrada = findViewById(R.id.tilEntrada);
        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btnDelete = findViewById(R.id.btnDelete);
        btnPunto = findViewById(R.id.btnPunto);
        btnMas = findViewById(R.id.btnMas);
        btnMenos = findViewById(R.id.btnMenos);
        btnMulti = findViewById(R.id.btnMulti);
        btnDivi = findViewById(R.id.btnDivi);
        btnCalcular = findViewById(R.id.btnCalcular);
        tvHistorial = findViewById(R.id.tvHistorial);


        //Evento Click de todos los botones
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tilEntrada.setText(tilEntrada.getText() + "0");
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tilEntrada.setText(tilEntrada.getText() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tilEntrada.setText(tilEntrada.getText() + "2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tilEntrada.setText(tilEntrada.getText() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tilEntrada.setText(tilEntrada.getText() + "4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + "9");
            }
        });

        btnPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(tilEntrada.getText() + ".");
            }
        });

        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(setOperador(tilEntrada.getText().toString(), "+"));
                isSum = true;
            }
        });

        btnMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(setOperador(tilEntrada.getText().toString(), "-"));
                isRest = true;
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(setOperador(tilEntrada.getText().toString(), "x"));
                isMul = true;
            }
        });

        btnDivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText(setOperador(tilEntrada.getText().toString(), "÷"));
                isDiv = true;
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilEntrada.setText("");
                tilEntrada.requestFocus();
            }
        });

        //Evento Click del calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(tilEntrada.getText()))
                {
                    dosCantidadesUnOperando(tilEntrada.getText().toString());

                }

            }
        });

    }

    private void dosCantidadesUnOperando(String textoIn)
    {

        //TODO: Fix bug si un numero tiene un menos truena.. ejemplo -25+2

        if (!TextUtils.isEmpty(textoIn))
        {

            String[] arrNumeros = textoIn.split("[^0-9.]");
            String[] arrOperando = textoIn.split("[0-9.]");
            boolean findOpera = false;
            String strOperando ="";

            for (String str : arrOperando)
            {
                if (str.equals("+") || str.equals("-") || str.equals("x") || str.equals("÷"))
                {
                    findOpera = true;
                    strOperando = str;
                }
            }

            if (arrNumeros.length == 2 && findOpera )
            {
                String result = "";

                try
                {
                    if (strOperando.equals("+"))
                        result = String.valueOf( Double.parseDouble(arrNumeros[0]) +  Double.parseDouble(arrNumeros[1]));
                    else if (strOperando.equals("-"))
                        result = String.valueOf( Double.parseDouble(arrNumeros[0]) -  Double.parseDouble(arrNumeros[1]));
                    else if (strOperando.equals("x"))
                        result = String.valueOf( Double.parseDouble(arrNumeros[0]) *  Double.parseDouble(arrNumeros[1]));
                    else if (strOperando.equals("÷"))
                    {
                        if (arrNumeros[1].equalsIgnoreCase("0") || arrNumeros[1].equalsIgnoreCase("."))
                            result = "Indefinido... No se puede dividir entre cero!";
                        else
                            result = String.valueOf( Double.parseDouble(arrNumeros[0]) /  Double.parseDouble(arrNumeros[1]));
                    }
                }
                catch (Exception ex)
                {
                    result = "Error, Invalid Expression";
                }

                tilEntrada.setText(result);

                //TODO: si todo esta bien guardar en historial
            }
            else
              Toast.makeText(getApplicationContext(),"Expresion Invalida ", Toast.LENGTH_LONG ).show();
        }
    }

    private String setOperador(String textoIn, String operador)
    {
        String result = "";

        if (textoIn != null)
        {
            if (TextUtils.isEmpty(textoIn))
            {
                result = "0" + operador;
            }
            else
            {

                if (!textoIn.contains("Indefinido"))
                {

                    //Verifico si el ultimo caracter ingresado es un operador... si lo es lo reemplazo sino lo agrego...
                    String ultimoCaracter = textoIn.substring(textoIn.length()-1);

                    if (!ultimoCaracter.equalsIgnoreCase(operador)) //Si son diferentes operadores
                    {
                        if (ultimoCaracter.equalsIgnoreCase("+") ||  ultimoCaracter.equalsIgnoreCase("-")
                                || ultimoCaracter.equalsIgnoreCase("x") ||  ultimoCaracter.equalsIgnoreCase("÷"))
                        {
                            result = textoIn.substring(0, textoIn.length()-1) + operador;

                        }
                        else
                            result = textoIn + operador;
                    }
                    else
                        result = textoIn;


                    String[] arrNumeros = result.split("[^0-9.]");


                    if (arrNumeros.length == 2)
                    {
                        if (isSum)
                            result = String.valueOf( Double.parseDouble(arrNumeros[0]) +  Double.parseDouble(arrNumeros[1]));
                        else if (isRest)
                            result = String.valueOf( Double.parseDouble(arrNumeros[0]) -  Double.parseDouble(arrNumeros[1]));
                        else if (isMul)
                            result = String.valueOf( Double.parseDouble(arrNumeros[0]) *  Double.parseDouble(arrNumeros[1]));
                        else if (isDiv)
                            result = String.valueOf( Double.parseDouble(arrNumeros[0]) /  Double.parseDouble(arrNumeros[1]));

                        result = result + operador;

                        if (isDiv)
                            if (arrNumeros[1].equalsIgnoreCase("0") || arrNumeros[1].equalsIgnoreCase("."))
                                result = "Indefinido... No se puede dividir entre cero!";

                    }

                }
                else
                    result = textoIn;
            }
        }

        //clear flags...

        isSum = false;
        isRest = false;
        isMul = false;
        isDiv = false;

        return result;
    }


}
