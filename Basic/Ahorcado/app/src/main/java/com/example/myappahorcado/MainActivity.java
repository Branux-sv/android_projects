package com.example.myappahorcado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Variables logica del juego..
    private String[] arrPalabraAhor = {"A","H","O","R","C","A","D","O"};
    private int contadorAhor = 0;
    private String palabraAhorcado = "";
    private String SecrectAhorcado = "AHORCADO";

    //Controles
    EditText edtLetra;
    Button btnValidar;
    Button btnLimpiar;
    TextView tvResultado;
    TextView tvLet1;
    TextView tvLet2;
    TextView tvLet3;
    TextView tvLet4;
    TextView tvLet5;
    TextView tvLet6;
    TextView tvLet7;
    TextView tvLet8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Enlazar controles
        edtLetra = findViewById(R.id.edtLetra);
        btnValidar = findViewById(R.id.btnValidar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        tvResultado = findViewById(R.id.tvResultado);
        tvLet1 = findViewById(R.id.tvLet1);
        tvLet2 = findViewById(R.id.tvLet2);
        tvLet3 = findViewById(R.id.tvLet3);
        tvLet4 = findViewById(R.id.tvLet4);
        tvLet5 = findViewById(R.id.tvLet5);
        tvLet6 = findViewById(R.id.tvLet6);
        tvLet7 = findViewById(R.id.tvLet7);
        tvLet8 = findViewById(R.id.tvLet8);


        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String caracterIn = edtLetra.getText().toString().trim();
                if (caracterIn.length() == 1)
                {
                    if (caracterIn.equalsIgnoreCase("E") || caracterIn.equalsIgnoreCase("L") ||
                            caracterIn.equalsIgnoreCase("C") || caracterIn.equalsIgnoreCase("T") ||
                            caracterIn.equalsIgnoreCase("I") || caracterIn.equalsIgnoreCase("V") ||
                            caracterIn.equalsIgnoreCase("A") )
                    {

                        if (caracterIn.equalsIgnoreCase("E"))
                        {
                            tvLet1.setText("E");
                            tvLet3.setText("E");
                        }

                        if (caracterIn.equalsIgnoreCase("L"))
                            tvLet2.setText("L");

                        if (caracterIn.equalsIgnoreCase("C"))
                            tvLet4.setText("C");

                        if (caracterIn.equalsIgnoreCase("T"))
                            tvLet5.setText("T");

                        if (caracterIn.equalsIgnoreCase("I"))
                            tvLet6.setText("I");

                        if (caracterIn.equalsIgnoreCase("V"))
                            tvLet7.setText("V");

                        if (caracterIn.equalsIgnoreCase("A"))
                            tvLet8.setText("A");

                        //Verificar si ya gano...
                        if (tvLet1.getText().toString().equalsIgnoreCase("E") && tvLet3.getText().toString().equalsIgnoreCase("E") &&
                                tvLet2.getText().toString().equalsIgnoreCase("L") && tvLet4.getText().toString().equalsIgnoreCase("C") &&
                                tvLet5.getText().toString().equalsIgnoreCase("T") && tvLet6.getText().toString().equalsIgnoreCase("I") &&
                                tvLet7.getText().toString().equalsIgnoreCase("V") && tvLet8.getText().toString().equalsIgnoreCase("A"))
                        {
                            Toast.makeText(getApplicationContext(),"Felicidades ha ganado el juego!!",Toast.LENGTH_LONG).show();
                            //limpiarTodo();
                        }


                    }
                    else //Se equivoco
                    {
                        if (!palabraAhorcado.equalsIgnoreCase(SecrectAhorcado))
                        {
                            palabraAhorcado += arrPalabraAhor[contadorAhor];
                            contadorAhor++;
                            tvResultado.setText("Resultado: " + palabraAhorcado);

                            //Verificar si ya perdio...
                            if (palabraAhorcado.equalsIgnoreCase(SecrectAhorcado))
                                Toast.makeText(getApplicationContext(),"Lo sentimos, perdio el juego... :( ",Toast.LENGTH_LONG).show();

                        }
                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"Por favor ingrese solo un caracter",Toast.LENGTH_LONG).show();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarTodo();
            }
        });

    }//Fin OnCreate

    private void limpiarTodo()
    {
        palabraAhorcado = "";
        contadorAhor = 0;
        tvResultado.setText("Resultado: ");
        edtLetra.setText("");
        edtLetra.requestFocus();
        tvLet1.setText(" X");
        tvLet2.setText(" X");
        tvLet3.setText(" X");
        tvLet4.setText(" X");
        tvLet5.setText(" X");
        tvLet6.setText(" X");
        tvLet7.setText(" X");
        tvLet8.setText(" X");
    }
}
